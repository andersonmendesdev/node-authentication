const https = require('https')
const fs = require('fs')
const credentials = {
    key: fs.readFileSync('./certificate/server.key'),
    cert: fs.readFileSync('./certificate/server.crt'),
    reqCert: false,
    resUnauthorized: false
}

const express = require('express')
const app = express()
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const User = require('./models/user')
const Noticias = require('./models/noticia')
const noticias = require('./routes/noticia')
const restrito = require('./routes/restrito')
const auth = require('./routes/auth')
const pages = require('./routes/pages')
const admin = require('./routes/admin')
const bodyParser = require('body-parser')
const port = process.env.PORT || 3000
const mongo = process.env.MONGODB || 'mongodb://localhost/noticias'
mongoose.Promise = global.Promise

//body parser
app.use(bodyParser.urlencoded({ extended: true}))

//session
app.use(session({ secret: 'andersonmendesdev' }))

//view engine and folder views
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

//public folder
app.use(express.static('public'))

//router
app.use('/', auth)
app.use('/', pages)
app.use('/noticias', noticias)
app.use('/restrito', restrito)
app.use('/admin', admin)

//create user root
const CreateInitialUser = async () => {
    const total = await User.count({})
    if( total === 0){
        const user = new User({
            username: 'user1',
            password: '12345',
            roles: ['restrito', 'admin']
        })
        await user.save()

        const user2 = new User({
            username: 'user2',
            password: '12345',
            roles: ['restrito']
        })
        await user2.save()

        console.log('created user sucess')
    }else{
        console.log('created user skiped')
    }
    /*
    const noticia = new Noticias({
        title: 'Noticia Publica'+new Date().getTime(),
        content: 'content',
        category: 'public'
    })
    await noticia.save()

    const noticia2 = new Noticias({
        title: 'Noticia privada'+new Date().getTime(),
        content: 'content',
        category: 'private'
    })
    await noticia2.save()*/
}

//start mongoose and app
const httpsServer = https.createServer(credentials, app)

mongoose
    .connect(mongo)
    .then(()=>{
        CreateInitialUser()
        httpsServer.listen(port, () => console.log('on in port: '+port+'\nmongodb in: '+mongo))
    })
    .catch(e => console.log(e))



