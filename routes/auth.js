const express = require('express')
const router = express.Router()
const User = require('../models/user')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const FacebookStrategy = require('passport-facebook').Strategy
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy

router.use(passport.initialize())
router.use(passport.session())

passport.serializeUser((user, done) =>{
    done(null, user)
})
passport.deserializeUser((user, done) => {
    done(null, user)
})

passport.use(new LocalStrategy(async(username, password, done) => {
    const user = await User.findOne({ username })
    if(user){
        const Isvalid = await user.checkPassword(password)
        if(Isvalid){
            return done(null, user)
        }else{
            return done(null, false)
        }
    }else{
        return done(null, false)
    }
}))
//strategy facebook
passport.use(new FacebookStrategy({
    clientID: '178859579486189',
    clientSecret: '3f772ff7289196f276920bf0fcd0c3af',
    callbackURL: 'https://localhost:3000/facebook/callback',
    profileFields: ['id', 'displayName', 'email', 'photos']
}, async(accessToken, refreshToken, profile, done) => {
    const userDB = await User.findOne({ facebookId: profile.id })
    if(!userDB){
        const user = new User({
            name: profile.displayName,
            facebookId: profile.id,
            roles: ['restrito']
        })
        await user.save()
        done(null, user)
    }else{
        done(null, userDB)
    }
}))
//google strategy
passport.use(new GoogleStrategy({
    clientID: '952674590416-k3cuopbq04t2gdgomko2q4t9u95181ii.apps.googleusercontent.com',
    clientSecret: 'CVIYPO-g9lxVBQ8_LsKzu2EO',
    callbackURL: 'https://localhost:3000/google/callback',
}, async(accessToken, refreshToken,err , profile, done) => {
    const userDB = await User.findOne({ googleId: profile.id })
    if(!userDB){
        const user = new User({
            name: profile.displayName,
            googleId: profile.id,
            roles: ['restrito']
        })
        await user.save()
        done(null, user)
    }else{
        done(null, userDB)
    }
}))


router.use((req, res, next) => {
    if(req.isAuthenticated()){
        res.locals.user = req.user
        if(!req.session.roles){
            req.session.roles = req.user.roles[0]
        }
        res.locals.roles = req.session.roles
    }
    next()
})

router.get('/change-role/:role', (req, res) =>{
    if(req.isAuthenticated()){
        if(req.user.roles.indexOf(req.params.role)>=0){
            req.session.roles = req.params.role
        }
    }
    res.redirect('/')
})

router.get('/login', (req, res) => {
    res.render('login')
})

router.get('/logout', (req, res) => {
    req.session.destroy(() =>{
        res.redirect('/')
    })
    
})

/*router.post('/login', async (req, res) => {
    const user = await User.findOne({ username: req.body.username })
    if(user){
        const Isvalid = await user.checkPassword(req.body.password)
        if(Isvalid){
            req.session.user = user
            req.session.roles = user.roles[0]
            res.redirect('/restrito/noticias')

        }else{
            res.redirect('/login')
        }
    }else{
        res.redirect('/login')
    }
})*/
//local passaport
router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: false
}))
//facebook route
router.get('/facebook', passport.authenticate('facebook'))
router.get('/facebook/callback', 
            passport.authenticate('facebook', {failureRedirect: '/'}),
            (req, res) => {
                res.redirect('/')
            }
)
//google route
router.get('/google', passport.authenticate('google', {scope: ['https://www.googleapis.com/auth/userinfo.profile']}))
router.get('/google/callback', 
            passport.authenticate('google', {failureRedirect: '/', successRedirect: '/'}),

)


module.exports = router