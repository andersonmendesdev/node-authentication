const mongoose = require('mongoose')
const  brcypt = require('bcrypt')

const userSchema = new mongoose.Schema({
    username: {
        type: String
    },
    password: {
        type: String
    },
    facebookId: String,
    googleId: String,
    name: String,
    roles:{
        type: [String],
        enum: ['restrito', 'admin']
    }
})

userSchema.pre('save', function(next){
    const user = this

    if(!user.isModified('password')){
        return next()
    }

    brcypt.genSalt((err, salt) => {
        brcypt.hash(user.password, salt, (err, hash) => {
            user.password = hash
            next()
        })
    })

})

userSchema.methods.checkPassword = function(password){
    return new Promise((resolve, reject) => {
        brcypt.compare(password, this.password, (err, isMatch) =>{
            if(err){
                reject(err)
            }else{
                resolve(isMatch)
            }
        })
    })    
}

const User = mongoose.model('User', userSchema)
module.exports = User