const express = require('express')
const router = express.Router()
const Noticias = require('../models/noticia')


router.get('/', async (req, res) => {
    let conditions = {}
    /*if(!('user' in req.session)){
        conditions = { category: 'public'}
    }*/
    conditions = { category: 'public'}
    const noticias = await Noticias.find( conditions )
    res.render('noticias/index', { noticias })
})


module.exports = router